import React, { createContext } from "react";
import styled from "styled-components";
import Cookies from "js-cookie";
import { User } from "../../types/user";

interface PageContextInterface {
  user: User | undefined;
}
export const PageContext = createContext<PageContextInterface | null>(null);

export default function PageContainer({
  children,
}: {
  children?: React.ReactNode;
}) {
  const userEmail: string = Cookies.get("logged_user") || "";
  
  return (
    <PageContext.Provider
      value={!!userEmail ? { user: { email: userEmail } } : null}
    >
      <PageContainerElement>{children}</PageContainerElement>
    </PageContext.Provider>
  );
}

const PageContainerElement = styled.div`
  display: flex;
  justify-content: center;
`;
