import React, { useState, useEffect, useContext } from "react";
import axios from "axios";
import styled from "styled-components";
import { Link, redirect, useNavigate } from "react-router-dom";
import { PageContext } from "../page-container";
import Cookies from "js-cookie";

// @TODO: Add styled components

export default function LoginForm({
  initialMode,
}: {
  initialMode: "login" | "register";
}) {
  const [mode, setMode] = useState(initialMode);
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const context = useContext(PageContext);
  const navigate = useNavigate()

  // Change mode with initialMode updates:
  useEffect(() => {
    if (initialMode !== mode) setMode(initialMode);
  }, [initialMode, setMode]);

  // @TODO Needs to check if user is already logged in using context

  const handleSubmit = (e: React.MouseEvent | React.FormEvent) => {
    e.preventDefault();

    axios
      .post(`/auth/${mode}`, {
        email,
        name,
        password,
      })
      .then((response) => {
        console.log(response);
        // Redirect the user back
        Cookies.set('logged_user', response.data.email)
        navigate("/");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <StyledLoginForm onSubmit={(e) => handleSubmit(e)}>
      <h1>{mode.charAt(0).toLocaleUpperCase() + mode.slice(1)}</h1>
      <StyledSmallText>
        Or {mode === "login" && <Link to="/register">register</Link>}
        {mode === "register" && <Link to="/login">login</Link>} instead.
      </StyledSmallText>
      <p>
        <label>Email:</label>
      </p>
      <StyledLoginFromInputs
        type="text"
        onChange={(e) => setEmail(e.target.value)}
        value={email}
      />

      {mode === "register" && (
        <>
          <p>
            <label>Name:</label>
          </p>
          <StyledLoginFromInputs
            type="text"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
        </>
      )}

      <p>
        <label>Password:</label>
      </p>
      <StyledLoginFromInputs
        type="password"
        onChange={(e) => setPassword(e.target.value)}
        value={password}
      />
      <StyledButtonContainer>
        <StyledButton onClick={(e) => handleSubmit(e)}>Submit</StyledButton>
      </StyledButtonContainer>
    </StyledLoginForm>
  );
}

const StyledLoginForm = styled.form`
  width: 300px;

  p {
    margin: 0 0 0.5em 0;
    padding: 0;
  }
`;

const StyledLoginFromInputs = styled.input`
  font-family: Consolas, "Courier New", monospace;
  padding: 0.25em 0.25em 0.3em;
  margin: 0 0 1em 0;
  border: 1px solid #ddd;
  border-radius: 4px;
  width: 100%;

  &:focus {
    outline: 0;
  }
`;

const StyledSmallText = styled.h3`
  color: #aaa;
  font-size: 12px;
  font-weight: 400;
  padding: 0.5em 0 1em;
`;

const StyledButtonContainer = styled.div`
  display: flex;
  justify-content: end;
`;

const StyledButton = styled.button`
  margin: 1em 0 0 0;
  padding: 0.25em 0.5em;
  border: 1px solid #ddd;
  border-radius: 4px;
  background-color: #111;
  color: #eee;
`;
