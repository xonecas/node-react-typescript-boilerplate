import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import LoginForm from "./components/login";
import PageContainer from "./components/page-container";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import axios from "axios";

//@TODO api url
axios.defaults.baseURL = "http://localhost:3001";
axios.defaults.headers["Content-Type"] = "application/json";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <PageContainer>
        <h1>Home</h1>
      </PageContainer>
    ),
  },
  {
    path: "/login",
    element: (
      <PageContainer>
        <LoginForm initialMode="login" />
      </PageContainer>
    ),
  },
  {
    path: "/register",
    element: (
      <PageContainer>
        <LoginForm initialMode="register" />
      </PageContainer>
    ),
  },
]);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
