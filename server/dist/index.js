"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("@koa/cors"));
const client_1 = require("@prisma/client");
const koa_1 = __importDefault(require("koa"));
const koa_body_1 = __importDefault(require("koa-body"));
const koa_json_1 = __importDefault(require("koa-json"));
const koa_logger_1 = __importDefault(require("koa-logger"));
const koa_router_1 = __importDefault(require("koa-router"));
const strip_ansi_1 = __importDefault(require("strip-ansi"));
const routes_1 = __importDefault(require("./auth/routes"));
const routes_2 = __importDefault(require("./health-check/routes"));
// try got and axios for server to server requests
// Create the client to let prisma manage the connection pool
const prisma = new client_1.PrismaClient();
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = new koa_1.default();
        const router = new koa_router_1.default();
        // Middleware:
        app.use((0, koa_logger_1.default)((str) => {
            // Remove colors for simplicity.
            console.log((0, strip_ansi_1.default)(str));
        }));
        app.use((0, koa_json_1.default)());
        app.use((0, koa_body_1.default)());
        app.use((0, cors_1.default)());
        // Share the connection for the db:
        app.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
            ctx.prisma = prisma;
            yield next();
        }));
        // Serve configured routes:
        app
            .use(routes_2.default.routes())
            .use(routes_2.default.allowedMethods());
        app.use(routes_1.default.routes()).use(routes_1.default.allowedMethods());
        // App starts
        const port = process.env.PORT || 3001;
        app.listen(port, () => console.log("Server listening on", port));
    });
}
main()
    .then(() => __awaiter(void 0, void 0, void 0, function* () {
    yield prisma.$disconnect();
}))
    .catch((e) => __awaiter(void 0, void 0, void 0, function* () {
    console.error(e);
    yield prisma.$disconnect();
    process.exit();
}));
