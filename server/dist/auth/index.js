"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.login = exports.register = void 0;
const client_1 = require("@prisma/client");
// @TODO: Use koa-passport + passport
function register(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const data = ctx.request.body;
            const user = yield ctx.prisma.user.create({
                data: data,
            });
            ctx.cookies.set("logged_user", user.email, { httpOnly: false });
            ctx.status = 201;
            ctx.response.body = user;
            next();
        }
        catch (e) {
            if (e instanceof client_1.Prisma.PrismaClientKnownRequestError) {
                // See prisma error codes here:
                // https://www.prisma.io/docs/reference/api-reference/error-reference#error-codes
                if (e.code === "P2002") {
                    ctx.status = 400;
                    ctx.response.body = { error: "Email already in use." };
                    next();
                }
            }
            else {
                throw e;
            }
        }
    });
}
exports.register = register;
function login(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const data = ctx.request.body;
        const user = yield ctx.prisma.user.findUnique({ where: { email: data.email } });
        if (!user) {
            ctx.status = 400;
            ctx.response.body = { error: "No user with specified email." };
        }
        if (data.password === (user === null || user === void 0 ? void 0 : user.password)) {
            ctx.cookies.set("logged_user", user === null || user === void 0 ? void 0 : user.email, { httpOnly: false });
            ctx.status = 200;
            ctx.response.body = user;
        }
        else {
            ctx.status = 404;
            ctx.response.body = { error: "Incorrect password." };
        }
        next();
    });
}
exports.login = login;
