"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_router_1 = __importDefault(require("koa-router"));
const _1 = require(".");
const router = new koa_router_1.default({ prefix: "/auth" });
router.post("/register", _1.register);
router.post("/login", _1.login);
exports.default = router;
