import Koa from "koa";
import { Prisma } from "@prisma/client";

// @TODO: Use koa-passport + passport

export async function register(ctx: Koa.ParameterizedContext, next: Koa.Next) {
  try {
    const data = ctx.request.body;
    const user = await ctx.prisma.user.create({
      data: data,
    });

    ctx.cookies.set("logged_user", user.email, { httpOnly: false });
    ctx.status = 201;
    ctx.response.body = user;

    next();
  } catch (e) {
    if (e instanceof Prisma.PrismaClientKnownRequestError) {
      // See prisma error codes here:
      // https://www.prisma.io/docs/reference/api-reference/error-reference#error-codes
      if (e.code === "P2002") {
        ctx.status = 400;
        ctx.response.body = { error: "Email already in use." };
        next();
      }
    } else {
      throw e;
    }
  }
}

export async function login(ctx: Koa.ParameterizedContext, next: Koa.Next) {
  const data = ctx.request.body;
  const user = await ctx.prisma.user.findUnique({ where: { email: data.email } });

  if (!user) {
    ctx.status = 400;
    ctx.response.body = { error: "No user with specified email." };
  }

  if (data.password === user?.password) {
    ctx.cookies.set("logged_user", user?.email, { httpOnly: false });
    ctx.status = 200;
    ctx.response.body = user;
  } else {
    ctx.status = 404;
    ctx.response.body = { error: "Incorrect password." };
  }

  next();
}
