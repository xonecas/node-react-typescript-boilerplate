import Router from "koa-router";
import Koa from "koa";

const router = new Router();

// health-check:
router.get(
  "/health-check",
  async (ctx: Koa.ParameterizedContext, next: Koa.Next) => {
    ctx.response.body = {
      data: "Hello World!",
    };
    ctx.response.status = 200;

    await next();
  }
);

export default router;
