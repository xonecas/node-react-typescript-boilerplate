import cors from "@koa/cors";
import { PrismaClient } from "@prisma/client";
import Koa from "koa";
import body from "koa-body";
import json from "koa-json";
import logger from "koa-logger";
import Router from "koa-router";
import stripAnsi from "strip-ansi";
import auth_router from "./auth/routes";
import health_check_router from "./health-check/routes"
// try got and axios for server to server requests

// Create the client to let prisma manage the connection pool
const prisma = new PrismaClient();

async function main() {
  const app = new Koa();
  const router = new Router();

  // Middleware:
  app.use(
    logger((str) => {
      // Remove colors for simplicity.
      console.log(stripAnsi(str));
    })
  );
  app.use(json());
  app.use(body());
  app.use(cors());

  // Share the connection for the db:
  app.use(async (ctx, next) => {
    ctx.prisma = prisma;
    await next();
  });

  // Serve configured routes:
  app
    .use(health_check_router.routes())
    .use(health_check_router.allowedMethods());
  app.use(auth_router.routes()).use(auth_router.allowedMethods());

  // App starts
  const port = process.env.PORT || 3001;
  app.listen(port, () => console.log("Server listening on", port));
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit();
  });
